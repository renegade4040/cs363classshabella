#!/bin/bash

file='*.tex'
wc='*.pdf'

for eachfile in $file
do
	pdflatex $eachfile
	rm *.log *.aux *.bib *.out 
	for i in $wc
		do
		wc i >wc.txt
	done
done


