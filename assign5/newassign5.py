import glob, sys


def Time(log):
	Worker = []
	Hour = 0
	Minute = 0
	for line in log.readlines():
		Worker.append(line)
	Name = Worker[0]
	Name = Name.strip()

	Worker = Worker[1:]
	for line in Worker:
		Day, Hr, Min = line.split()

		Hr = int(Hr[:1])
		Hour =  Hour + Hr
		Min = int(Min[:2])
		Minute = Minute + Min

	Hr, Minute = divmod(Minute, 60)
	Hour = Hour + Hr
	return Name, Hour, Minute

for file in glob.glob("*.csv"):

	TData = open(file, "r")
	Name, Hour, Minute = Time(TData)

	item =  "Total Time Worked By: " + Name +  " is " +  str(Hour) + " Hrs and " +str(Minute) + " Mins"
	print item
