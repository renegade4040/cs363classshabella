#include <math.h> // Allows me to use the pow(x,y) --> x^y

/* This is a bunch of stuff to use in pixel mapping. */

int InCircle(int totalRows, int totalCols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not: 1 is yes, 0 is no.

    int centerX = totalCols / 2;
    int centerY = totalRows / 2;
    int dist    = 0;              //Distance from center to pixel.
    int lefteye = 0;
    int righteye = 0;
    int mouth = 0;
    dist = pow((pow(centerX - pixCol,2)) + (pow(centerY - pixRow,2)), 0.5);
    lefteye = pow((pow((centerX / 1.35) - pixCol,2)) + (pow((centerY / 1.75) - pixRow,2)), 0.5);
    righteye = pow((pow((centerX * 1.25) - pixCol,2)) + (pow((centerY / 1.75) - pixRow,2)) , 0.5);
    mouth = pow((pow(centerX - pixCol,2)) + (pow((centerY * 1.2) - pixRow,2)), 0.5);

    if (dist < radius) {
        InOrOut = 1;
    }

    if(lefteye < (radius / 8)) {
	    InOrOut = 2;
    }  
    
    if (righteye < (radius / 8)) {
	  InOrOut = 2;
    }
     if (mouth < (radius / 4)) {
          InOrOut = 2;
    }

    return InOrOut;
}

int InStem(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0; //Integer flag for being in the stem.
          
    int brow = (totalrows / 2) - radius;
    int trow = (totalrows / 2) - (radius + 40);

    int lcol = (totalcols / 2) - 35;
    int rcol = (totalcols / 2) + 35;
    
    if(pixRow > trow && pixRow < brow) {
	if(pixCol > lcol && pixCol < rcol) {
           InOrOut = 1;
	}
    }

   return InOrOut;    
}

